# Mautic

## Setting up with vitess

```
vtctlclient ApplySchema --sql="$(cat schemas/mautic.sql)" mautic
vtctlclient ApplyVSchema --vschema="$(cat schemas/mautic-vschema.json)" mautic
```

## Installing

TODO: fix the initial installation steps (currently you need to comment out `mailer_from_name` from the configMap.yaml)

### Production

`helm -n mautic install mautic . --values=values-prod.yaml`

### Sandbox
`helm -n mautic install mautic . --values=values-sandbox.yaml`

## Upgrading

### Production

`helm -n mautic upgrade mautic . --values=values-prod.yaml`

### Sandbox
`helm -n mautic upgrade mautic . --values=values-prod.yaml`